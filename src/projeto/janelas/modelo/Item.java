package projeto.janelas.modelo;

public class Item {
	
	private String id;
	private String idProduto;
	private String valorTotal;
	
	public String getId() {
		return id;
	}
	public void setId (String id) {
		this.id = id;
	}
	
	public String getIdProduto() {
		return idProduto;
	}
	public void setIdProduto (String idProduto) {
		this.idProduto = idProduto;
	}
	
	public String getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal (String valorTotal) {
		this.valorTotal = valorTotal;
	}

}
